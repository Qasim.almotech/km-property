<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    // The System Title Farsi version...
    'SystemTitle' => 'سیستم توزیع زمین',
    // The Header Farsi version...
    'searchFor' => 'جستجو',
    'settings' => 'تنظیمات',
    'logout' => 'خروج',
    'km-system' => 'سیستم توزیع زمین',
    // the sidebar Farsi version...
    'dashboard' => 'داشبورد',
    'property' => 'زمین ها',
    'listProperty' => 'لست زمین ها',
    'addProperty' => 'اضافه نمودن زمین',
    'charts' => 'گراف ها',
    'tables' => 'جداول',
    'pages' => 'صفحات',
    'authentication' => 'اجازه عبور',
    'login' => 'ورود',
    'register' => 'راجستر',
    'forgotPassword' => 'رمز عبورم فراموش شده',
    // The main body(cpanel) Farsi version...
      'dashboardCpanel' => 'صفحه کنترول',
      // The login Farsi version...
      'loginToSystem' =>'ورود به سیستم',
      'emailAddress'=>'ایمیل آدرس',
      'password'=>'رمز عبور',
      'rememberPassword'=>'رمز عبور را به خاطر بسپارید',
      'forgotPassword'=>'رمز عبور فراموش گردیده؟',
      // The properties Farsi version...
      'propertyList'=> 'لست زمین ها',
      'register_property'=> 'ثبت زمین',
      'personal_information'=> 'معلومات شخصی',
      'property_details'=> 'معلومات زمین',
      'first_name'=> 'نام',
      'last_name'=> 'تخلص',
      'father_name'=> 'نام پدر',
      'grandfather_name'=> 'نام پدر کلان',
      'address'=> 'آدرس',
      'tazkira_no'=> 'نمبر تذکره',
      'property_type'=> 'نوع زمین',
      'select_property_type'=> 'انتخاب نوع زمین ...',
      'property_size'=> 'مساحت زمین',
      'property_no'=> 'نمبر زمین',
      'property_location'=> 'موقعیت زمین',
      'description'=> 'توضیحات',
      'save'=> 'ثبت',
      'residential'=> 'رهایشی',
      'business'=> 'تجارتی',
      'neighbor'=> 'حالات اربعه',
      'east'=> 'شرق',
      'north'=> 'شمال',
      'south'=> 'جنوب',
      'west'=> 'غرب',


];
