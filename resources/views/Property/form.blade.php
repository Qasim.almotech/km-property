@extends('layout.app')
@section('main-content')

    <main>

      <div class="container-fluid">
          <div class="row">
            <!-- // the first div -->
              <div class="col-lg-12">
                  <div class="card shadow-sm border-1 rounded-lg mt-3">
                      <div class="card-header" >
                        <h3 class=" font-weight-light">{{ __('general.register_property') }}</h3>
                      </div>
                      <!-- first card body -->
                      <div class="card-body">
                          <form action="{{Route('dashboard')}}" method="GET">
                            <div class="row mb-1">
                              <div class="col-md-3">
                                <h8> {{ __('general.personal_information') }}</h8>
                                <hr/>

                                <div class="form-floating mb-3 mb-md-10">
                                    <input class="form-control"  type="text" placeholder="first name" />
                                    <label for="inputFirstName">{{ __('general.first_name') }}</label>
                                </div>

                                <div class="form-floating mb-3 mb-md-10">
                                    <input class="form-control" id="inputLastName" type="text" placeholder="Enter your last name" />
                                    <label for="inputLastName">{{__('general.last_name')}}</label>
                                </div>

                                <div class="form-floating mb-3 mb-md-10">
                                    <input class="form-control" id="inputFatherName" type="text" placeholder="Father Name" />
                                    <label for="inputFatherName">{{__('general.father_name')}} </label>
                                </div>

                                <div class="form-floating mb-3 mb-md-10">
                                    <input class="form-control" id="inputGrandFatherName" type="text" placeholder="GrandFather Name"/>
                                    <label for="inputGrandFatherName">{{__('general.grandfather_name')}} </label>
                                </div>

                                <div class="form-floating mb-3 mb-md-10">
                                    <input class="form-control" id="inputAddress" type="text" placeholder="Address" />
                                    <label for="inputAddress">{{__('general.address')}}</label>
                                </div>

                                <div class="form-floating mb-3 mb-md-10">
                                    <input class="form-control" id="inputTazkiraNo" type="number" placeholder="Tazkira No" />
                                    <label for="inputTazkiraNo">{{__('general.tazkira_no')}}</label>
                                </div>

                              </div>
                              <div class="col-md-3" >
                                <h8> {{ __('general.property_details') }}</h8>
                                <hr/>

                                <div class="form-floating mb-3 mb-md-10">
                                    <input class="form-control" id="inputPropertyNo" type="number" placeholder="Property No" />
                                    <label for="inputPropertyNo"> {{__('general.property_no')}} </label>
                                </div>

                                <div class="form-floating mb-3 mb-md-10">
                                    <input class="form-control" id="inputLastName" type="text" placeholder="Enter your last name" />
                                    <label for="inputLastName"> {{__('general.property_size')}}  </label>
                                </div>

                                <div class="form-floating mb-3 mb-md-10">
                                    <input class="form-control" id="inputPropertyLocation" type="text" placeholder="Property Location" />
                                    <label for="inputPropertyLocation" > {{__('general.property_location')}} </label>
                                </div>



                                <div class="form-floating mb-3 mb-md-10">
                                     <select class="form-control" id="type">
                                       <option>
                                         {{__('general.select_property_type')}}
                                       </option>
                                       <option>
                                         {{__('general.business')}}
                                       </option>
                                       <option>
                                         {{__('general.residential')}}
                                       </option>
                                     </select>
                                     <label for="type">{{ __('general.property_type') }}</label>
                                </div>

                                <div class="form-floting">
                                    <textarea  class="form-control"  rows="5" placeholder="{{__('general.description')}}"></textarea >


                                </div>


                              </div>
                              <div class="col-md-3" >
                                <h8> {{ __('general.neighbor') }}</h8>
                                <hr/>

                                <div class="form-floating mb-3 mb-md-10">
                                    <input class="form-control" id="inputPropertyNo" type="number" placeholder="Property No" />
                                    <label for="inputPropertyNo"> {{__('general.north')}} </label>
                                </div>

                                <div class="form-floating mb-3 mb-md-10">
                                    <input class="form-control" id="inputLastName" type="text" placeholder="Enter your last name" />
                                    <label for="inputLastName"> {{__('general.east')}}  </label>
                                </div>

                                <div class="form-floating mb-3 mb-md-10">
                                    <input class="form-control" id="inputLastName" type="text" placeholder="Enter your last name" />
                                    <label for="inputLastName"> {{__('general.south')}}  </label>
                                </div>

                                <div class="form-floating mb-3 mb-md-10">
                                    <input class="form-control" id="inputLastName" type="text" placeholder="Enter your last name" />
                                    <label for="inputLastName"> {{__('general.west')}}  </label>
                                </div>
                                <hr/>

                                <h8>مساحت</h8>
                                <div class="form-floating mb-1 mb-md-10 mt-2">
                                    <input class="form-control" id="inputLastName" type="number" placeholder="Enter your last name" />
                                    <label for="inputLastName">اندازه</label>
                                </div>
                                <!-- chick list -->
                                <div class="row mb-1">
                                  <div class="col-md-3">
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                                    <label class="form-check-label " for="flexRadioDefault1">
                                      بسواسه
                                    </label>
                                  </div>
                                </div>
                                <div class="col-md-3">
                                  <div class="form-check">
                                    <input class="form-check-input " type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked>
                                    <label class="form-check-label " for="flexRadioDefault2">
                                      بسوه
                                    </label>
                                  </div>
                                </div>
                                <div class="col-md-3">
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked>
                                    <label class="form-check-label" for="flexRadioDefault2">
                                      جریب
                                    </label>
                                  </div>
                                </div>
                              </div>

                              </div>

                            </div>

                              <hr/>
                              <div class="row mb-1">
                                <div class="col-md-1">
                                  <div class="mt-1 mb-0">
                                  <div class="d-grid col-md-12">
                                    <input  type="submit" class="btn btn-primary btn-block" value="{{__('general.save')}}" />
                                  </div>
                                </div>
                              </div>

                              <div class="col-md-1">
                                <div class="mt-1 mb-0">
                                  <div class="d-grid col-md-12">
                                    <input  type="submit" class="btn btn-primary btn-block" value="ثبت و ادامه" />
                                  </div>
                                </div>
                              </div>
                            </div>

                          </form>
                      </div>
                  </div>
              </div>

          </div>
      </div>


    </main>
@endsection
