<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PropController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () { return view('welcome');});
Route::get('/Cpanel', [PropController::class, 'Navigate'])->name('dashboard');
Route::get('/', [PropController::class, 'backToLogin']);
Route::get('/form', [PropController::class, 'ViewForm'])->name('property.create');
Route::get('properties', [PropController::class, 'ListProperty'])->name('ListProperty');
Route::get('register',function(){return view('register');});

Route::get('cpanel', function(){ return view ('Cpanel');});
Route::get('login', function() { return view('login');});
